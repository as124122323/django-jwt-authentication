from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import AuthenticationFailed
import jwt
from jwt import exceptions
from django.conf import settings
import logging
from django_redis import get_redis_connection

redis_conn = get_redis_connection('default')
logger = logging.getLogger(__name__)


class MyJWTAuthentication(BaseAuthentication):

    def authenticate(self, request):
        """
        - 必須定義此方法
        - 有三種返回值
            - 拋出異常，此API請求後續不再執行
            - 回傳一個元組，有兩個值組成 (val1, val2)
                - 後續在視圖中調用request.user => 為val1
                - 後續在視圖中調用request.auth => 為val2
            - 回傳None => 代表不做事，交給下一個認證類處理
        """
        salt = settings.SECRET_KEY

        token = request.query_params.get('token')
        redis_ret = redis_conn.get(token)
        logger.info("redis_ret: %s" % redis_ret)

        if redis_ret is not None:
            raise AuthenticationFailed({'code': 400, 'msg': 'token已註銷'})

        try: 
            payload = jwt.decode(token, salt, algorithms=["HS256"])
        except exceptions.ExpiredSignatureError:
            raise AuthenticationFailed({'code': 400, 'msg': 'token已過期'})
        except jwt.DecodeError:
            raise AuthenticationFailed({'code': 400, 'msg': 'token認證失敗'})
        except jwt.InvalidTokenError:
            raise AuthenticationFailed({'code': 400, 'msg': '非法的token'})
        
        logger.info("payload: %s" % payload)
        return (payload, token)

