from django.urls import path

from . import views

urlpatterns = [
    path('logout_token', views.logoutTokenView.as_view(), name='logout_token'),
    path('refresh_token', views.RefreshTokenView.as_view(), name='refresh_token'),
    path('check_token', views.CheckTokenView.as_view(), name='check_token'),
    path('login', views.UserLoginView.as_view(), name='user_login'),
    path('register', views.UserRegisterView.as_view(), name='user_register'),
]