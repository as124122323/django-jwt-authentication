from django.http import JsonResponse
from django.contrib import auth
from django.forms.models import model_to_dict
import logging
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import views
from .extensions import MyJWTAuthentication
from .utils.jwt_auth import create_token
from django_redis import get_redis_connection
import datetime

redis_conn = get_redis_connection('default')
logger = logging.getLogger(__name__)
User = auth.get_user_model()


class UserLoginView(views.APIView):
    """
        Description: 用戶登入，並返回jwt
    """
    # Authentications是定義要以什麼機制判斷是否登入
    # Permissions才是真的決定了是否拒絕訪問請求
    authentication_classes = ()
    permission_classes = ()
 
    def post(self, request):
        # request.data 可以處理json, from_data, x-www-form-urlencoded的POST參數
        # json為Dict格式；from_data, x-www-form-urlencoded為QueryDict格式
        post_data = request.data
        if type(request.data) != dict:
            post_data = post_data.dict()
            
        user = auth.authenticate(**post_data)  # 先驗證
        if user and user.is_active:  # 驗證成功且處於激活狀態則登入
            jwt_token = create_token(
                payload = {
                    "name": user.username,
                    "is_active": user.is_active,
                    "is_staff": user.is_staff,
                    "is_superuser": user.is_superuser
                },
                timeout = 2
            )
            return JsonResponse({"status": "success", "data": {"jwt_token": jwt_token}, "message": None})
        return JsonResponse({"status": "failed", "data": None, "message": "password is fault, or %s is not register or not active" % post_data["username"]})


class CheckTokenView(views.APIView):
    """
        Description: 檢查Token
    """
    authentication_classes = (MyJWTAuthentication, )
    permission_classes = ()
 
    def get(self, request):
        payload = request.user  # MyJWTAuthentication中的payload
        logger.info("payload: %s" % payload)
        return JsonResponse({"status": "success", "data": payload, "message": None})


class RefreshTokenView(views.APIView):
    """
        Description: 刷新Token(返回有新的到期時間的jwt)
    """
    authentication_classes = (MyJWTAuthentication, )
    permission_classes = ()
 
    def get(self, request):
        payload = request.user  # MyJWTAuthentication中的payload
        del payload["exp"]
        jwt_token = create_token(
            payload = payload,
            timeout = 2
        )
        logger.info("payload: %s" % payload)
        return JsonResponse({"status": "success", "data": {"jwt_token": jwt_token}, "message": None})

class logoutTokenView(views.APIView):
    """
        Description: 註銷Token(不能真的註銷，只能將註銷的記到redis裡)
    """
    authentication_classes = (MyJWTAuthentication, )
    permission_classes = ()
 
    def get(self, request):
        token = request.query_params.get('token')
        payload = request.user  # MyJWTAuthentication中的payload
        expire_timestamp = payload["exp"]
        now_timestamp = int(datetime.datetime.utcnow().timestamp())

        if expire_timestamp > now_timestamp:
            difference = expire_timestamp - now_timestamp
            logger.info("expire_time: %s" % difference)

            redis_conn.set(token, "")
            redis_conn.expire(token, difference)

        return JsonResponse({"status": "success", "data": None, "message": None})


class UserRegisterView(views.APIView):
    """
        Description: 用戶註冊
    """
    authentication_classes = ()
    permission_classes = ()
 
    def post(self, request):
        post_data = request.data
        if type(request.data) != dict:
            post_data = post_data.dict()

        user_obj_list = User.objects.filter(username=post_data["username"])
        if len(user_obj_list) > 0:
            msg = "this username (%s) has been registered" % post_data["username"]
            logger.info(msg)
            return JsonResponse({"status": "failed", "message": msg})
        User.objects.create_user(**post_data)
        return JsonResponse({"status": "success"})
